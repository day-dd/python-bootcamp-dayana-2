# -*- coding: utf-8 -*-
import math


def cal_kuh_lator(n) -> str:
    """Task01
    Write unittests for this function.
    Hint: be careful, the parameter has no type annotations so make sure you test for other data types than usually expected 

    """

    if n > 0:
        digits = int(math.log10(n))+1
        s = "m"
        e = "h"
    elif n == 0:
        return "möh"
    else:
        digits = int(math.log10(-n))+1
        s = "h"
        e = "m"
    
    for _ in range(1, digits+1):
        s += "u"
    s += e
    return s


def d_and_d_spell_book(name: str, level: int, target: str) -> callable:
    """Task02
    Write unittests for this function

    """

    return{
        "ignis pila": lambda: f"cast fireball level {level} at {target}",
        "cataracta": lambda: f"cast waterfall level {level} at {target}",
        "stereo": lambda: f"cast chill house music at volume level {level} from 2 sides at {target}"
    }[name]


def query_db(*args, **kwargs) -> int:
    """
    you probably wanna mock me :)
    """
    raise Exception('cause I can')


def query_book_metadata(isbn) -> dict:
    """Task03
    Write unittests for this function

    """

    return {
        "title": query_db('SELECT RANDOM BOOK TITLE'),
        "author": query_db('SELECT RANDOM AUTHOR NAME')
    }