# -*- coding: utf-8 -*-
import inspect
from assignment02.typing import untyped_function, strings_to_numbers, define_types_for_this_function


def test_untyped_function_annotations():
    annotations = inspect.get_annotations(untyped_function)
    assert 'value1' in annotations and 'value2' in annotations and 'return' in annotations
    assert isinstance(annotations['value1'], UnionType)
    assert isinstance(annotations['value2'], UnionType)
    assert isinstance(annotations['return'], UnionType)


def test_untyped_function():
    result = untyped_function(range(-99, 0))
    assert result == sum(range(0, 100))
    assert not bool(untyped_function((1.5, -1.5), value2=3.1415))


def test_strings_to_numbers():
    result = strings_to_numbers(
        ('0', '-1', '1', '99.999', '3,1415', '1J', '5+5j', 'Sith Lord', f'{1j*1j}'))
    assert result == [0, -1, 1, 99.999, None, 1j, 5+5j, None, -1+0j]


def test_define_types_for_this_function_annotations():
    # print(inspect.get_annotations(get_diff_elements_between_list))
    # {'l1': Cadena, 'l2': Cadena, 'return': <class 'list'>}
    annotations = inspect.get_annotations(define_types_for_this_function)
    assert 'x' in annotations and 'y' in annotations and 'return' in annotations
    try:
        from assignment02.typing import Foo
        from assignment02.typing import Bar
        from assignment02.typing import Baz
    except:
        assert False
    assert type(annotations['x']) == type(Foo)
    assert type(annotations['y']) == type(Bar)
    assert type(annotations['return']) == type(Baz)


def test_define_types_for_this_function_implementation():
    x1 = ['alpha', 'bravo', 'charlie', 'delta', 'echo']
    x2 = ['foxtrott', 'golf']
    y = {
        'bravo': ('clapping hands'),
        'charlie': ['chaplin']
    }
    result1 = define_types_for_this_function(x1, y)
    assert len(result1) == 2
    assert 'bravo' in result1 and 'charlie' in result1
    result2 = define_types_for_this_function(x2, y)
    assert result2 is None
